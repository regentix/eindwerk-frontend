import React from "react";
import ReactDOM from "react-dom";
import "./core/index.css";
import App from "./core/App";

ReactDOM.render(<App />, document.getElementById("root"));
