import React, { Props, Component } from "react";
import Nav from "./Nav";

const Layout = ( { children }: Props<Component>) => {
  return (
      <div>
          <Nav theme={'light'} mode={'horizontal'}/>
          { children }
      </div>
  );
};

export default Layout;
