import React from "react";
import { BrowserRouter as Router , Route, Switch } from "react-router-dom";

export default () => {
  return (
    <Router>
      <Switch>
        <Route
          path={"/login"}
          render={() => <h2>Login</h2>}
        />
      </Switch>
    </Router>
  );
};
