import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import Router from './Router';
import Layout from './components/Layout';

const App: React.FC = () => {
  return (
    <Layout>
      <Router />
    </Layout>
  );
}

export default App;
